package fenetres;

import java.awt.Label;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class IMCFrame {

	
	String[] listeOptions = { "Homme", "Femme" };
	JComboBox<String> combobox = new JComboBox<>(listeOptions);
	Label lbl_m = new Label("M/Mm : ");
	Label lbl_taille = new Label("Ma Taille : ");
	Label lbl_poids = new Label("Mon Poids : ");
	Label lbl_imc = new Label("Mon IMC : ");
	
	Label erreur = new Label();
	
	
	JTextField txt_taille = new JTextField(7);
	JTextField txt_poids = new JTextField(7);
	JTextField txt_imc = new JTextField(7);
	
	
	String choix = null;
	double taille=0.0,poids=0.0;
	public IMCFrame() {

		// Création d'une fenetre
		JFrame f = new JFrame("Calcul IMC");
		f.setSize(500, 250);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Ajout de panneau
		JPanel panel = new JPanel();
		f.setContentPane(panel);
		panel.setLayout(null);

		// Ajout de combobox
		combobox.addItemListener(new ItemHandler());
		panel.add(combobox);
		
		
		lbl_m.setBounds(25, 25, 100, 25);
		panel.add(lbl_m);
		combobox.setBounds(130, 25, 150, 25);
		panel.add(combobox);
		
		lbl_taille.setBounds(25, 60, 100, 25);
		panel.add(lbl_taille);
		
		txt_taille.setBounds(130, 60, 150, 25);
		panel.add(txt_taille);
		erreur.setBounds(200, 60, 100, 25);
		panel.add(erreur);
		
		lbl_poids.setBounds(25, 100, 100, 25);
		panel.add(lbl_poids);
		txt_poids.setBounds(130, 100, 150, 25);
		panel.add(txt_poids);
		
		lbl_imc.setBounds(25, 140, 100, 25);
		panel.add(lbl_imc);
		txt_imc.setBounds(130, 140, 150, 25);
		panel.add(txt_imc);
		

		f.setVisible(true);

	}

	public static void main(String[] args) {
		
		IMCFrame imcFrame = new IMCFrame();
    
	}
	
	private class ItemHandler implements ItemListener{
		
		@Override
		public void itemStateChanged(ItemEvent event) {
			if(event.getSource() == combobox) {
				choix = combobox.getSelectedItem().toString();
			}
		}
	}
	
	private class KeyClique implements KeyListener{

		@Override
		public void keyPressed(KeyEvent event) {
			
			
		}

		@Override
		public void keyReleased(KeyEvent event) {
			if(event.getSource()== txt_taille) {
				try {
				taille = Double.parseDouble(txt_taille.getText());
				}catch(NumberFormatException e) {
					
				}
				
			}
			
		}

		@Override
		public void keyTyped(KeyEvent event) {
			// TODO Auto-generated method stub
			
		}
		
	}
}
